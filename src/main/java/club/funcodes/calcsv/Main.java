// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.calcsv;

import static org.refcodes.cli.CliSugar.*;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Delimiter;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.CsvStringRecordReader;
import org.refcodes.tabular.CsvStringRecordWriter;
import org.refcodes.tabular.Header;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;

public class Main {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "calcsv";
	private static final String TITLE = "<<< Calc-CSV >>>";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/bin2txt_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Small and friendly tool doing some useful calculations on CSV (JMeter results) data. Makes heavy use of the  REFCODES.ORG artifacts found together with the FUNCODES.CLUB sources at <http://bitbucket.org/refcodes>.";
	private static final String DEST_FILE = "destinationFile";
	private static final String SOURCE_FILE = "sourceFile";
	private static final String DEST_DELIMITER = "destinationDelimiter";
	private static final String SOURCE_DELIMITER = "sourceDelimiter";
	private static final String RANGE_COLUMN_NAME = "rangeColumnName";
	private static final String DIST_COLUMN_NAME = "distColumnName";
	private static final String GROUP_BY_COLUMN_NAME = "groupByColumnName";
	private static final String GRANULARITY = "granularity";
	private static final String STEP = "step";
	private static final String PERCENT = "percent";
	private static final String TOTAL = "total";
	private static final String HEADER = "header";
	private static final char DEFAULT_DELIMITER = Delimiter.CSV.getChar();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final Option<String> theSourcePathArg = stringOption( 's', "source-file", SOURCE_FILE, "The source CSV file from which to read the values." );
		final Option<Character> theSourceDelimiterrArg = charOption( "source-delimiter", SOURCE_DELIMITER, "The delimiter to use for parsing the source CSV file." );
		final Option<String> theRangeArg = stringOption( "range", RANGE_COLUMN_NAME, "The range (min and max values) found for the given column." );
		final Option<String> theDistArg = stringOption( "distribution", DIST_COLUMN_NAME, "The distribution of values for the given column." );
		final Option<String> theGroupByArg = stringOption( "group-by", GROUP_BY_COLUMN_NAME, "The column used to group the results by." );
		final Option<Integer> theGranularityArg = intOption( "granularity", GRANULARITY, "Number of divisions your data is to be consolidated into." );
		final Option<Integer> theStepArg = intOption( "step", STEP, "Step width to be used when your data is to be consolidated." );
		final Option<String> theDestPathArg = stringOption( 'd', "destination-file", DEST_FILE, "The destination CSV file to which to write the values." );
		final Option<Character> theDestDelimiterArg = charOption( "destination-delimiter", DEST_DELIMITER, "The delimiter to use when generating the destination CSV file." );
		final Flag thePercentFlag = new Flag( "percent", PERCENT, "Do transform the result to percentages." );
		final Flag theTotalFlag = new Flag( "total", TOTAL, "The total exceeding a given value." );
		final Flag theHeaderFlag = new Flag( "header", HEADER, "When set, write out a header line." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases (
			and(
				theSourcePathArg, optional( theSourceDelimiterrArg, theVerboseFlag ),
				xor(
					and (
						theRangeArg,
						optional (
							theGroupByArg,
							theHeaderFlag,
							theDestPathArg,
							theDestDelimiterArg
						)
					),
					and(
						theDistArg, optional(
							theHeaderFlag,
							xor(
								theStepArg,
								theGranularityArg
							),
							thePercentFlag,
							theTotalFlag,
							theDestPathArg,
							theDestDelimiterArg
						)
					)
				)
			),
			xor( theHelpFlag, theSysInfoFlag )
		);
		final Example[] theExamples = examples(
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		try {

			// -----------------------------------------------------------------
			// INIT:
			// -----------------------------------------------------------------

			final boolean isVerbose = theVerboseFlag.isEnabled();
			final boolean isPercent = thePercentFlag.isEnabled();
			final boolean isTotal = theTotalFlag.isEnabled();
			final String theSourcePath = theArgsProperties.get( theSourcePathArg );
			File theSourceFile = null;
			if ( theSourcePath != null && theSourcePath.length() != 0 ) {
				if ( isVerbose ) {
					LOGGER.info( "Using source file <\"" + theSourcePath + "\"> ..." );
				}
				theSourceFile = new File( theSourcePath );
			}
			final char theSourceDelimiter = theArgsProperties.getCharOr( theSourceDelimiterrArg, DEFAULT_DELIMITER );
			final String theDestPath = theArgsProperties.get( theDestPathArg );
			File theFestFile = null;
			if ( theDestPath != null && theDestPath.length() != 0 ) {
				if ( isVerbose ) {
					LOGGER.info( "Using destination file <\"" + theDestPath + "\"> ..." );
				}
				theFestFile = new File( theDestPath );
			}
			final char theDestDelimiter = theArgsProperties.getCharOr( theDestDelimiterArg, DEFAULT_DELIMITER );

			// -----------------------------------------------------------------
			// MAX:
			// -----------------------------------------------------------------

			if ( theRangeArg.hasValue() ) {
				try ( CsvStringRecordReader reader = new CsvStringRecordReader( theSourceFile, theSourceDelimiter ); CsvStringRecordWriter rangeWriter = ( theFestFile != null ) ? new CsvStringRecordWriter( theFestFile, theDestDelimiter ) : new CsvStringRecordWriter( System.out, theDestDelimiter ) ) {
					if ( theGroupByArg.hasValue() ) {
						final Map<String, Range> ranges = getRange( reader, theRangeArg.getValue(), theGroupByArg.getValue(), theVerboseFlag.getValue() );
						if ( theHeaderFlag.isEnabled() ) {
							rangeWriter.writeHeader( theGroupByArg.getValue() + ", min", "max" );
						}
						final List<String> keys = new ArrayList<>( ranges.keySet() );
						Range eRange;
						Collections.sort( keys );

						for ( String eKey : keys ) {
							eRange = ranges.get( eKey );
							rangeWriter.writeNext( eKey, toPlainString( eRange.min ), toPlainString( eRange.max ) );
						}
					}
					else {
						final Range range = getRange( reader, theRangeArg.getValue(), theVerboseFlag.getValue() );
						if ( theHeaderFlag.isEnabled() ) {
							rangeWriter.writeHeader( "min", "max" );
						}
						rangeWriter.writeNext( toPlainString( range.min ), toPlainString( range.max ) );
					}
				}
			}
			// -----------------------------------------------------------------
			// DISTRIBUTION
			// -----------------------------------------------------------------
			if ( theDistArg.hasValue() ) {

				try ( CsvStringRecordReader rangeReader = new CsvStringRecordReader( theSourceFile, theSourceDelimiter ) ) {
					final Range theRange = getRange( rangeReader, theDistArg.getValue(), theVerboseFlag.getValue() );
					int theGranularity = 100;
					if ( theGranularityArg.hasValue() ) {
						theGranularity = theGranularityArg.getValue();
					}
					if ( isVerbose ) {
						LOGGER.info( "range(" + theDistArg.getValue() + ") = " + theRange );
					}
					try ( CsvStringRecordReader distReader = new CsvStringRecordReader( theSourceFile, theSourceDelimiter ); CsvStringRecordWriter distWriter = ( theFestFile != null ) ? new CsvStringRecordWriter( theFestFile, theDestDelimiter ) : new CsvStringRecordWriter( System.out, theDestDelimiter ) ) {
						writeDist( distReader, theDistArg.getValue(), theRange, theGranularity, theStepArg.getValueOr( -1 ), distWriter, isPercent, isTotal, isVerbose, theHeaderFlag.isEnabled() );
					}
				}
			}

		}
		catch ( Exception e ) {
			LOGGER.error( Trap.asMessage( e ), e );
			System.exit( e.hashCode() % 0xFF );
		}
	}

	private static void writeDist( CsvStringRecordReader theReader, String theColumnName, Range theRange, int theGranularity, int theStep, CsvStringRecordWriter theWriter, boolean isPercent, boolean isTotal, boolean isVerbose, boolean isHeader ) {
		if ( theStep != -1 ) {
			theGranularity = (int) ( ( theRange.max - theRange.min ) / theStep ) + 1;
		}
		final double[] theRanges = new double[theGranularity];
		final int[] theAggregate = new int[theGranularity];
		final double theRangeSize = theStep != -1 ? theStep : ( theRange.max - theRange.min ) / ( theGranularity - 1 );
		double eRange = theRange.min;

		if ( isVerbose ) {
			LOGGER.info( "Range size = " + theRangeSize );
		}

		for ( int i = 0; i < theGranularity; i++ ) {
			theRanges[i] = eRange;
			eRange += theRangeSize;
			theAggregate[i] = 0;
		}
		double eValue;
		int count = 0;
		while ( theReader.hasNext() ) {
			org.refcodes.tabular.Record<String> eNext = theReader.next();
			eValue = Double.parseDouble( eNext.get( theColumnName ) );
			int index = (int) ( ( eValue - theRange.min ) / theRangeSize );
			if ( index < 0 || index >= theAggregate.length ) {
				LOGGER.warn( "Index <" + index + "> out of bounds <" + theAggregate.length + ">!" );
			}
			else {
				theAggregate[index]++;
			}
			count++;
		}
		if ( isVerbose ) {
			LOGGER.info( "Total number of lines processed = " + count + "" );
		}
		if ( isHeader ) {
			if ( isPercent ) {
				theWriter.writeHeader( ( isTotal ? "n > " : "" ) + "Range", "%" );
			}
			else {
				theWriter.writeHeader( ( isTotal ? "n > " : "" ) + "Range", "Sum" );
			}
		}
		double ePercent;
		for ( int i = theAggregate.length - 1; i >= 0; i-- ) {

			if ( isTotal ) {
				if ( i < theAggregate.length - 1 ) {
					theAggregate[i] += theAggregate[i + 1];
				}
			}
			eRange = theRanges[i];
			if ( isPercent ) {
				ePercent = ( (double) 100 ) / ( (double) count ) * theAggregate[i];
				theWriter.writeNext( toPlainString( eRange ), toPlainString( ePercent ) );
			}
			else {
				theWriter.writeNext( toPlainString( eRange ), "" + theAggregate[i] );
			}
		}
	}

	private static String toPlainString( double range ) {
		return new BigDecimal( range ).toPlainString().replace( '.', ',' );
	}

	private static Range getRange( CsvStringRecordReader reader, String columnName, boolean isVerbose ) throws IOException {
		if ( isVerbose ) {
			LOGGER.info( "Using column <" + columnName + ">." );
		}
		final Header<String> theSourceHeader = reader.readHeader();
		final Column<?> column = theSourceHeader.get( columnName );
		if ( column == null ) {
			throw new RuntimeException( "There is no such column <" + columnName + ">." );
		}
		Range range = new Range();
		double eValue;
		int count = 0;
		while ( reader.hasNext() ) {
			org.refcodes.tabular.Record<String> eNext = reader.next();
			eValue = Double.parseDouble( eNext.get( columnName ) );
			if ( range.max == null || eValue > range.max ) {
				range.max = eValue;
			}
			if ( range.min == null || eValue < range.min ) {
				range.min = eValue;
			}
			count++;
		}
		if ( isVerbose ) {
			LOGGER.info( "Total number of lines processed = " + count + "" );
		}
		return range;
	}

	private static Map<String, Range> getRange( CsvStringRecordReader reader, String columnName, String groupByColumnName, boolean isVerbose ) throws IOException {
		if ( isVerbose ) {
			LOGGER.info( "Using column <" + columnName + "> and group by <" + groupByColumnName + ">." );
		}
		final Header<String> theSourceHeader = reader.readHeader();
		final Column<?> column = theSourceHeader.get( columnName );
		if ( column == null ) {
			throw new RuntimeException( "There is no such column <" + columnName + ">." );
		}
		final Map<String, Range> ranges = new HashMap<String, Range>();
		double eValue;
		String eGroupBy;
		int count = 0;
		Range eRange;
		while ( reader.hasNext() ) {
			org.refcodes.tabular.Record<String> eNext = reader.next();
			eValue = Double.parseDouble( eNext.get( columnName ) );
			eGroupBy = eNext.get( groupByColumnName );
			eRange = ranges.get( eGroupBy );
			if ( eRange == null ) {
				eRange = new Range();
				ranges.put( eGroupBy, eRange );
			}
			if ( eRange.max == null || eValue > eRange.max ) {
				eRange.max = eValue;
			}
			if ( eRange.min == null || eValue < eRange.min ) {
				eRange.min = eValue;
			}
			count++;
		}
		if ( isVerbose ) {
			LOGGER.info( "Total number of lines processed = " + count + "" );
		}
		return ranges;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class Range {
		Double min = null;
		Double max = null;

		@Override
		public String toString() {
			return "(" + min + " .. " + max + ")";
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////
}
