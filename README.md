# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The `funcodes-calcsv` repository is a small and  friendly tool doing some useful calculations on CSV (JMeter results) data and demonstrating the usage of the [`refcodes-tabular`](https://bitbucket.org/refcodes/refcodes-tabular) artifact.***

## Getting started ##

To get up and running, clone the [`funcodes-calcsv`](https://bitbucket.org/funcodez/funcodes-calcsv/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-calcsv)'s `git` repository. 

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-calcsv/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-calcsv.git
```

Using `CSV`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`CSV`](https://bitbucket.org/funcodez/funcodes-calcsv/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-calcsv.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 


## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-calcsv/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-calcsv/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/csv-launcher-x.y.z.sh
```

The resulting `csv.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/csv-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

### Starting csv ###

As described above, you may go with directly executing the `JAR` file by invoking `java -jar target/funcodes-calcsv-0.0.1.jar` or, after some doings, you may also go with the big fat executable bash script like `./target/csv-launcher-x.y.z.sh` to start the [`csv`](https://bitbucket.org/funcodez/funcodes-calcsv/src/master/src/main/java/club/funcodes/csv/Main.java).

Either way you launch the [`csv`](https://bitbucket.org/funcodez/funcodes-calcsv/src/master/src/main/java/club/funcodes/csv/Main.java) server, you will be greeted with an ASCII art banner and a log output. We assume you are in the `target` folder and use the `csv.sh` script: 

```
./csv.sh --help # Show a help message
```

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-calcsv/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
